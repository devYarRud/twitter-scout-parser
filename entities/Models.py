from sqlalchemy import Column, Integer, String, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TwitterRaw(Base):
    __tablename__ = 'twitter_raw'
    id = Column(Integer, primary_key=True)
    twitter_id = Column(String)
    username = Column(String)
    content = Column(String)
    media_type = Column(String)
    embed_code = Column(String)
    number_of_comments = Column(Integer)
    number_of_retweets = Column(Integer)
    number_of_likes = Column(Integer)
    created_at = Column(TIMESTAMP)
    tweeted_at = Column(TIMESTAMP)


class TwitterFollowsMapping(Base):
    __tablename__ = 'twitter_follows_mapping'
    id = Column(Integer, primary_key=True)
    userID = Column(Integer)
    twitter_handle = Column(String)
    account_type = Column(Integer)
