from repositories.TwitterAccountsRepository import TwitterAccountsRepository
from time import sleep
import logging

logging.basicConfig(level=logging.DEBUG, filename="logfile.log", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")


class TwitterAccHandler:
    account = TwitterAccountsRepository()
    acc_count = 0

    def need_reload(self):
        accounts = self.get_all_accounts()
        if len(accounts) != self.acc_count:
            self.acc_count = len(accounts)
            return True

        return False

    def get_all_accounts(self):
        try:
            all = self.account.get_all()
            return all
        except Exception as e:
            logging.warning("get all acc sleep 1 s")
            sleep(1)
            logging.exception(e)
            all = self.account.get_all()
            return all

    def set_acc_count(self):
        accounts = self.get_all_accounts()
        self.acc_count = len(accounts)

    def get_accounts_ids(self, twitter_api):
        ids = []
        accounts = self.get_all_accounts()

        for account in accounts:
            if account.twitter_handle is not None:
                try:
                    user = twitter_api.get_user(screen_name=account.twitter_handle)
                    ids.append(str(user.id))
                except Exception as e:
                    logging.exception(e)
                    logging.debug(account.twitter_handle)
                    continue
        logging.info(ids.__str__())
        return ids
        # user = twitter_api.get_user(screen_name="Yarosla56040251")
        # return [str(user.id)]

    def get_account_by_name(self, name):
        return self.account.get_by_username(name)