import logging
import redis
import tweepy
import json

logging.basicConfig(level=logging.DEBUG, filename="logfile.log", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")


class MyStreamListener(tweepy.StreamListener):
    acc_ids = None
    storage = redis.Redis(host='localhost', port=6379, db=0)

    def on_status(self, status):
        self.push_to_redis(status)
        if hasattr(status, "retweeted_status"):
            # print("retweet")
            logging.info("retweet")
            logging.info("status_id: " + str(status.id))
            self.push_to_storage("retweet", status)
            # print("PUSH RETWEET MESSAGE TO REDIS")

    def set_acc_ids(self, arr_ids):
        self.acc_ids = arr_ids

    def push_to_redis(self, twitter_response):
        if hasattr(twitter_response, "retweeted_status") is False and twitter_response.user.id_str in self.acc_ids:
            logging.info(str(twitter_response.text))
            logging.info(str(twitter_response.user.screen_name))
            logging.info("PUSH TWITTER MESSAGE TO REDIS")
            self.push_to_storage("tweet", twitter_response)

    def push_to_storage(self, status, response):
        json_response = json.dumps(response._json)
        key = str(status) + "_" + response.id_str
        value = json_response
        self.storage.set(key, value)

    def on_error(self, status_code):
        logging.warning("error")
        logging.warning(str(status_code))
        if status_code == 420:
            # returning False in on_error disconnects the stream
            return False