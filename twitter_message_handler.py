import datetime
from time import sleep
import redis
import tweepy
import json
from dateutil import parser
from repositories.TwitterMessageRepository import TwitterMessageRepository
from configparser import ConfigParser, ExtendedInterpolation, os
conf_parser = ConfigParser(interpolation=ExtendedInterpolation())
conf_parser.read(os.path.abspath("config.ini"))

auth = tweepy.OAuthHandler(conf_parser["twitter"]["consumer_key"], conf_parser["twitter"]["consumer_secret"])

auth.set_access_token(conf_parser["twitter"]["access_token"], conf_parser["twitter"]["access_token_secret"])

api = tweepy.API(auth)

storage = redis.Redis(host='localhost', port=6379, db=0)

db_twitter_raw = TwitterMessageRepository()


def get_storage_data_by_keys():
    for key in storage.scan_iter():
        # print(key)
        value = storage.get(key).decode('utf-8')
        key_data = prepare_key(key.decode('utf-8'))
        if need_download(key_data):
            save_message(key_data, key, value)


def need_download(key_data):
    if key_data['tittle'] == "tweet":
        return True
    elif key_data['tittle'] == "retweet":
        return True

    return False


def save_message(key_data, key, value):
    sleep(1)
    if key_data["tittle"] == "tweet":
        try:

            message = json.loads(value)

            media_type = ""

            try:
                for entity_key, entity_value in message['extended_entities'].items():
                    print(entity_key)
                    print(entity_value)
                    if entity_key == "media":
                        for i, media in enumerate(entity_value):
                            if i == 0:
                                media_type += media["type"]
                            else:
                                if media_type != media["type"]:
                                    media_type += "," + media["type"]
            except Exception as e:
                print(e)

            if media_type == "":
                media_type = None

            tweeted_at = parser.parse(message["created_at"])
            data = {
                "content": message["text"],
                "username": message["user"]["screen_name"],
                "tweeted_at": tweeted_at,
                "number_of_likes": int(message["favorite_count"]),
                "number_of_retweets": int(message["retweet_count"]),
                "number_of_comments": int(message["quote_count"]),
                "media_type": media_type,
                "twitter_id": message["id_str"]
            }

            try:
                data['content'] = message["extended_tweet"]["full_text"]
            except Exception as e:
                print(e)

            message_from_db = db_twitter_raw.get_by_twitter_id(data["twitter_id"])
            if message_from_db is None:
                db_twitter_raw.create(data)
                print("twitter message save")
            else:
                storage.delete(key)
                print("twitter key delete")
                print(key)
            # print(data)
        except Exception as e:
            print(e)
            storage.delete(key)
            print("key delete")
            print(key)
    elif key_data["tittle"] == "retweet":
        try:
            message = json.loads(value)

            if "retweeted_status" in message:
                db_twitter_raw.update(message["retweeted_status"]["id_str"], {
                    "number_of_retweets": message["retweeted_status"]["retweet_count"],
                    "number_of_likes": message["retweeted_status"]["favorite_count"],
                    "number_of_comments": message["retweeted_status"]["quote_count"]
                })
            storage.delete(key)
            print("retweet key delete")
            print(key)
        except Exception as e:
            print("exeption in retweet")
            print(e)
            storage.delete(key)
            print("retweet key delete")
            print(key)


def prepare_key(key):
    tittle = key.split('_')[0]
    message_id = key.split('_')[1]

    return { "tittle": tittle, "message_id": message_id }


def checker():
    while True:
        get_storage_data_by_keys()
        sleep(4)


checker()