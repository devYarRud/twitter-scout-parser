from configparser import ConfigParser, ExtendedInterpolation
from services.twitter_accounts_service import TwitterAccHandler
from time import sleep
from services.stream import MyStreamListener
import tweepy
import logging
import sys
import os

parser = ConfigParser(interpolation=ExtendedInterpolation())
parser.read(os.path.abspath("config.ini"))
logging.basicConfig(level=logging.INFO, filename="logfile.log", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")


auth = tweepy.OAuthHandler(parser["twitter"]["consumer_key"], parser["twitter"]["consumer_secret"])
auth.set_access_token(parser["twitter"]["access_token"], parser["twitter"]["access_token_secret"])

twitter_api = tweepy.API(auth)

accounts = TwitterAccHandler()

db_account_ids = accounts.get_accounts_ids(twitter_api)
accounts.set_acc_count()

myStreamListener = MyStreamListener()
myStreamListener.set_acc_ids(db_account_ids)


def restart_script():
    python = sys.executable
    os.execl(python, python, *sys.argv)


if db_account_ids is not []:
    myStream = tweepy.Stream(auth=twitter_api.auth, listener=myStreamListener)
    try:
        myStream.filter(follow=db_account_ids, is_async=True)
    except Exception as e:
        logging.warning("error")
        logging.exception(e)
    while True:
        if myStream.running is False:
            myStream.disconnect()
            logging.warning("myStream.running is False")
            logging.warning("RESTART")
            restart_script()

        if accounts.need_reload():
            myStream.disconnect()
            logging.warning("restart thread")
            restart_script()

        sleep(3)
else:
    sleep(10)
    logging.warning("db_account_ids is empty")
    logging.warning("restart script")
    restart_script()