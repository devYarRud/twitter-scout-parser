from time import sleep
from db_connection import DB
from sqlalchemy import exc
from entities.Models import TwitterRaw
from random import randrange
import datetime
import logging


logging.basicConfig(level=logging.DEBUG, filename="logfile.log", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")


class TwitterMessageRepository():
    db = None

    def __init__(self):
        self.db = DB()

    def get_all(self):
        try:
            all_messages = self.db.get_session().query(TwitterRaw).all()
            self.db.get_session().commit()
            return all_messages
        except exc.DBAPIError as e:
            logging.exception(e)
            self.db.get_session().rollback()
            if e.connection_invalidated:
                logging.debug('**************invalid connection get_all twitter raw')
                logging.debug('reconnect')
                sleep(randrange(3))
                self.db.reconnect()
                return self.db.get_session().query(TwitterRaw).all()

    def get_by_twitter_id(self, id):
        try:
            self.db.get_session().commit()
            row = self.db.get_session().query(TwitterRaw).filter_by(twitter_id=id).first()
            return row
        except exc.DBAPIError as e:
            self.db.get_session().rollback()
            if e.connection_invalidated:
                logging.debug('**************invalid connection in get_by_twitter_id')
                logging.debug('reconnect')
                self.db.reconnect()
                return self.db.get_session().query(TwitterRaw).filter_by(twitter_id=id).first()

    def get(self, id):
        try:
            row = self.db.get_session().query(TwitterRaw).filter_by(id=id).first()
            self.db.get_session().commit()
            return row
        except exc.DBAPIError as e:
            self.db.get_session().rollback()
            if e.connection_invalidated:
                logging.debug('**************invalid connectio')
                logging.debug('reconnect')
                self.db.reconnect()
                return self.db.get_session().query(TwitterRaw).filter_by(id=id).first()

    def create(self, data):
        logging.debug("Create row in TwitterMessageRepository")
        message = TwitterRaw(
            content=data['content'] if 'content' in data else None,
            embed_code=data['embed_code'] if 'embed_code' in data else None,
            media_type=data['media_type'] if 'media_type' in data else None,
            number_of_comments=data['number_of_comments'] if 'number_of_comments' in data else None,
            number_of_likes=data['number_of_likes'] if 'number_of_likes' in data else None,
            number_of_retweets=data['number_of_retweets'] if 'number_of_retweets' in data else None,
            tweeted_at=data['tweeted_at'] if 'tweeted_at' in data else None,
            twitter_id=data['twitter_id'] if 'twitter_id' in data else None,
            username=data['username'] if 'username' in data else None,
            created_at=datetime.datetime.now(),
        )

        try:
            self.db.get_session().add(message)
            self.db.get_session().commit()
            return message
        except Exception as e:
            self.db.get_session().rollback()
            logging.exception(e)
            logging.debug('**************invalid connection in create ')
            logging.debug('reconnect')
            sleep(randrange(3))
            self.db.reconnect()

            self.db.get_session().add(message)
            self.db.get_session().commit()

            return message

    def update(self, twitter_id, data):
        message = self.get_by_twitter_id(twitter_id)

        if message is not None:
            if "number_of_retweets" in data:
                message.number_of_retweets = data['number_of_retweets']
            if "number_of_likes" in data:
                message.number_of_likes = data['number_of_likes']
            if "number_of_comments" in data:
                message.number_of_comments = data['number_of_comments']

            # print(data)
            try:
                self.db.get_session().commit()

                return message
            except exc.DBAPIError as e:
                if e.connection_invalidated:
                    self.db.get_session().rollback()
                    print('**************invalid connection')
                    print('reconnect')
                    self.db.reconnect()

                    self.db.get_session().commit()

                    return message

    def delete(self, id):
        message = self.get(id=id)
        self.db.get_session().delete(message)

        return True