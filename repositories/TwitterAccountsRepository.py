from time import sleep
from db_connection import DB
from sqlalchemy import exc
from entities.Models import TwitterFollowsMapping
from random import randrange
import datetime
import logging

logging.basicConfig(level=logging.DEBUG, filename="logfile.log", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")


class TwitterAccountsRepository():
    db = None

    def __init__(self):
        self.db = DB()

    def get_all(self):
        try:
            self.db.get_session().commit()
            all_accounts = self.db.get_session().query(TwitterFollowsMapping).all()
            return all_accounts
        except exc.DBAPIError as e:
            logging.exception(e)
            if e.connection_invalidated:
                logging.debug('**************invalid connection get_all twitter acc')
                logging.debug('reconnect')
                sleep(randrange(3))
                self.db.reconnect()
                return self.db.get_session().query(TwitterFollowsMapping).all()

    def get(self, id):
        try:
            row = self.db.get_session().query(TwitterFollowsMapping).filter_by(id=id).first()
            self.db.get_session().commit()
            return row
        except exc.DBAPIError as e:
            if e.connection_invalidated:
                logging.debug('**************invalid connectio')
                logging.debug('reconnect')
                self.db.reconnect()
                return self.db.get_session().query(TwitterFollowsMapping).filter_by(id=id).first()

    def create(self, data):
        account = TwitterFollowsMapping(
            account_type=data['account_type'] if 'account_type' in data else None,
            twitter_handle=data['twitter_handle'] if 'twitter_handle' in data else None,
            userID=data['user_id'] if 'user_id' in data else None,
        )

        try:
            self.db.get_session().add(account)
            self.db.get_session().commit()

            return account
        except exc.DBAPIError as e:
            if e.connection_invalidated:
                logging.debug('**************invalid connection in create TwitterAccountsRepository')
                logging.debug(datetime.datetime.now())
                logging.debug('reconnect')
                self.db.reconnect()

                self.db.get_session().add(account)
                self.db.get_session().commit()

                return account

    def update(self, id, data):
        account = self.get(id)

        try:
            self.db.get_session().commit()

            return account
        except exc.DBAPIError as e:
            if e.connection_invalidated:
                print('**************invalid connection')
                print('reconnect')
                self.db.reconnect()

                self.db.get_session().commit()

                return account

    def delete(self, id):
        account = self.get(id=id)
        self.db.get_session().delete(account)

        return True

    def get_by_username(self, username):
        try:
            self.db.get_session().commit()
            row = self.db.get_session().query(TwitterFollowsMapping).filter_by(twitter_handle=username).first()
            if row is None:
                row = self.db.get_session().query(TwitterFollowsMapping).filter_by(twitter_handle="@"+username).first()
            return row
        except exc.DBAPIError as e:
            logging.exception(e)
            if e.connection_invalidated:
                logging.debug('**************invalid connection in get account by username')
                logging.debug('reconnect')
                sleep(randrange(2))
                self.db.reconnect()
                row = self.db.get_session().query(TwitterFollowsMapping).filter_by(twitter_handle=username).first()
                if row is None:
                    row = self.db.get_session().query(TwitterFollowsMapping).filter_by(
                        twitter_handle="@" + username).first()
                return row