from sqlalchemy import create_engine, exc
from sqlalchemy.orm import sessionmaker
from configparser import ConfigParser, ExtendedInterpolation, os
parser = ConfigParser(interpolation=ExtendedInterpolation())
parser.read(os.path.abspath("config.ini"))


class DB:
    connection_string = 'mysql+mysqldb://{user}:{password}@{host}:{port}/{dbname}?charset=utf8mb4'.format(
        user=parser['db_config']['user'],
        password=parser['db_config']['password'],
        host=parser['db_config']['host'],
        port=parser['db_config']['port'],
        dbname=parser['db_config']['dbname'])

    engine = create_engine(connection_string, pool_pre_ping=True, pool_recycle=3600)
    connected_engine = engine.connect()
    Session = sessionmaker(bind=connected_engine)
    session = Session()

    def get_session(self):
        return self.session

    def reconnect(self):
        self.connected_engine = self.engine.connect()
        Session = sessionmaker(bind=self.connected_engine)
        self.session = Session()
        return self.get_session()